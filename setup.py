from distutils.core import setup

setup(name='pkgman',
      version='0.1',
      description='Python and AUR wrapper',
      url='https://bitbucket.org/angrysoft/pkg',
      author='Sebastian Zwierzchowski',
      author_email='sebastian.zwierzchowski@gmail.com',
      license='GPL2',
      package_dir={'pkgman' : 'pkgman'},
      packages=['pkgman'],
      scripts=['pkg.py'])