# Copyright 2015 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com>
# pkg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess

class PkgMan:
    """Class Pkgman"""

    def __init__(self, debug=False):
        """Constructor for Pkgman"""
        self.pmanager = ''
        self.debug = debug
        self.verbose = False
        self.power = None

    def _printDebug(self, *args):
        """_printDebug"""
        if self.debug:
            print('Debug: ', args)

    def _cmd(self, args, pkglist=[], sudo=False):
        """search"""
        a = []
        if sudo:
            a.append('/usr/bin/sudo')
        a.append(self.pmanager)
        a.append(args)
        if type(pkglist) == list or type(pkglist) == tuple:
            if pkglist != []:
                a.extend(pkglist)
        else:
            a.append(pkglist)
        self._printDebug(a)
        return subprocess.call(a)

    def _sudoCmd(self, args, pkglist=[]):
        """_suodCmd"""
        return self._cmd(args, pkglist, sudo=True)

    def setVerbose(self, bool):
        """setVerbose"""
        self.verbose = bool



class Aptitude(PkgMan):
    """Class Aptitude"""

    def __init__(self):
        """Constructor for Aptitude"""
        super(Aptitude, self).__init__()
        self.pmanager = '/usr/bin/aptitude'

class Aptget(PkgMan):
    """Class Aptget"""

    def __init__(self):
        """Constructor for Aptget"""
        super(Aptget, self).__init__()
        self.pmanager = '/usr/bin/apt-get'

    def sysUpdate(self):
        """sysUpdate"""
        self.update()
        self._sudoCmd('dist-upgrade')
        if self.power == 'poweroff':
            subprocess.call(['systemctl', 'poweroff'])
        elif self.power == 'reboot':
            subprocess.call(['systemctl', 'reboot'])
    def search(self, pname):
        """search"""
        self.pmanager = '/usr/bin/apt-cache'
        self._cmd('search', pname)

    def update(self):
        """update"""
        self._sudoCmd('update')

    def upgrade(self):
        """upgrade"""
        self._sudoCmd('upgrade')

    def remove(self, pname):
        """remove"""
        self._sudoCmd('remove', pname)

    def info(self, pname):
        """info"""
        self.pmanager = '/usr/bin/apt-cache'
        self._cmd('showpkg', pname)

    def install(self, pname):
        """install"""
        self._sudoCmd('install', pname)