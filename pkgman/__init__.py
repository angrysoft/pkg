class Pkg:
    name = ''
    version = ''
    rel = ''
    depends = []
    optdepends = []
    makedepends = []
    description = ''
    url = ''
    urlpath = ''
    license = ''
    conflicts = ''
    provides = ''
    replaces = ''
    maintainer = ''
    srcpkg = ''

    def __init__(self, **data):
        for d in ('name', 'version', 'rel', 'depends', 'optdepends', 'makedepends', 'description'):
            if d in data:
                setattr(self, d, data[d])

    def from_aur(self, data):
        for d in data:
            setattr(self, d.lower(), data[d])
        if self.version:
            self.version, self.rel = self.version.rsplit('-', 1)

    def __eq__(self, pkg):
        if isinstance(pkg, Pkg):
            if pkg.version == self.version and pkg.rel == self.rel:
                return True
            else:
                return False
        else:
            return False

    def __str__(self):
        return f'{self.name}-{self.version}-{self.rel}'

    def __repr__(self):
        return f'{self.name}-{self.version}-{self.rel}'
