# Copyright 2015 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com>
# pkg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import tarfile
import os
import subprocess
import configparser
from pkgman import Pkg


class PacmanDb:
    """Class PacmanDb"""
    def __init__(self):
        """Constructor for PacmanDb"""
        self.dbPath = '/var/lib/pacman'
        self.dbLocal = {}
        self.dbSync = {}

    def loadLocalDb(self):
        """loadLocal"""
        for d in os.listdir(os.path.join(self.dbPath, 'local')):
            if d == 'ALPM_DB_VERSION':
                continue
            name, version, rel = d.rsplit('-', 2)
            self.dbLocal[name] = {}
            self.dbLocal[name]['version'] = version
            self.dbLocal[name]['rel'] = rel
            # self.dbLocal[pkgname].update(self.parseDescFile(os.path.join(self.dbPath, 'local', d, 'desc')))

    def loadTarDb(self, dbname):
        """loadTarDb"""
        ret = subprocess.run(['tar', 'tf', os.path.join(self.dbPath, 'sync', dbname)], stdout=subprocess.PIPE)
        if ret.returncode == 0:
            pkgs = ret.stdout.decode()

            for p in pkgs.split('\n'):
                pkg = os.path.dirname(p)
                if not pkg or pkg == 'ALPM_DB_VERSION':
                    continue
                name, version, rel = pkg.rsplit('-', 2)
                if name in self.dbSync:
                    continue
                self.dbSync[name] = {}
                self.dbSync[name]['version'] = version
                self.dbSync[name]['rel'] = rel

    def loadSyncDb(self):
        """loadSyncDb"""
        config = configparser.ConfigParser(allow_no_value=True)
        config.read('/etc/pacman.conf')
        for repo in config.sections():
            if repo == 'options':
                continue
            self.loadTarDb(f'{repo}.db')

    @staticmethod
    def parseDescFile(fname):
        """parseDescFile"""
        data = {}
        section = ''
        with open(fname) as desc:
            for line in desc:
                line = line.strip()
                if line == '':
                    continue
                if line[0] == '%':
                    section = line.strip('%')
                    section = section.lower()
                    data[section] = []
                else:
                    data[section].append(line)
        return data
    #TODO : query local db
    #TODO : query sync db

    def find_aur_pkg(self):
        self.loadLocalDb()
        self.loadSyncDb()
        aur_pkg = []
        for pkg in self.dbLocal:
            if pkg not in self.dbSync:
                ret = Pkg(**self.dbLocal[pkg])
                ret.name = pkg
                yield ret


class Pacman:
    """Class Pacman"""

    def __init__(self):
        """Constructor for Pacman"""
        self._pacmanExec = 'pacman'
        self._verbose = False
        self.power = None
        self.debug = True
        self._noConfirm = False
        self._pacmanArgs = list()
        self._pkgList = list()

    def _printDebug(self, *args):
        """_printDebug"""
        if self.debug:
            print('Debug: ', args)

    def addArg(self, arg):
        self._pacmanArgs.append(arg)

    def addPkgs(self, pkg):
        if type(pkg) == list:
            self._pkgList.extend(pkg)
        else:
            self._pkgList.append(pkg)

    def _makePacmanArgs(self):
        self._pacmanArgs.insert(0, self._pacmanExec)
        if self._verbose:
            self.addArg('--verbose')
        if self._noConfirm:
            self.addArg('--noconfirm')

        self._pacmanArgs.extend(self._pkgList)

    def _resetArgs(self):
        self._pacmanArgs.clear()

    def _callPacman(self):
        """_callPacman"""
        self._makePacmanArgs()
        self._printDebug(self._pacmanArgs)
        ret = subprocess.run(self._pacmanArgs)
        self._resetArgs()
        return ret

    @property
    def verbose(self):
        """setVerbose"""
        return self._verbose

    @verbose.setter
    def verbose(self, value):
        self._verbose = value

    @property
    def noConfirm(self):
        return self._noConfirm

    @noConfirm.setter
    def noConfirm(self, value):
        self._noConfirm = value

    def systemUpgrade(self):
        """sysUpdate"""
        self.addArg('-Syu')
        self._callPacman()

    def search(self, pkgs):
        """search"""
        self.addArg('-Ss')
        self.addPkgs(pkgs)
        self._callPacman()

    def update(self):
        """update"""
        self.addArg('-Sy')
        self._callPacman()

    def upgrade(self):
        """upgrade"""
        self.addArg('-Su')

    def remove(self, pkgs):
        """remove"""
        self.addArg('-Rnsu')
        self.addPkgs(pkgs)
        self._callPacman()

    def info(self, pkgs):
        """info"""
        self.addArg('-Si')
        self.addPkgs(pkgs)
        self._callPacman()

    def install(self, pkgs):
        """install"""
        self.addArg('-S')
        self.addPkgs(pkgs)
        self._callPacman()
