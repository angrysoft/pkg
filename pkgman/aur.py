# Copyright 2015 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com>
# pkg
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import subprocess
import urllib.request
import urllib.error
import json
from .pacman import PacmanDb
from . import Pkg
from shutil import rmtree


class Git:
    """Class Git"""

    def __init__(self):
        """Constructor for Git"""
        self.git = '/usr/bin/git'

    def _git(self, cmd, args=''):
        """search"""
        a = list()
        a.append('/usr/bin/git')
        a.append(cmd)
        if args:
            a.append(args)
        return subprocess.call(a)

    def pull(self):
        """update"""
        self._git('pull')

    def clone(self, path):
        """clone"""
        self._git('clone', path)


class Aur:
    """Class Aur"""

    def __init__(self):
        """Constructor for Aur"""
        self.aur_sycn_path = '/var/lib/pacman/aur'
        self.aur_cache_path = '/var/cache/pacman/aur'
        if not os.path.exists(self.aur_sycn_path):
            os.makedirs(self.aur_sycn_path)
        if not os.path.exists(self.aur_cache_path):
            os.makedirs(self.aur_cache_path)
        self.git = Git()
        self.url = 'https://aur.archlinux.org'
        self.rpc = f'{self.url}/rpc.php'
        self.proto_version = 5
        self.pacmandb = PacmanDb()

    def sync_pkg(self, pkg):
        pkg_aur = self.info(pkg.name)
        pkg_sync_path_old = f'{self.aur_sycn_path}/{pkg.name}-{pkg.version}'
        pkg_sync_path = f'{self.aur_sycn_path}/{pkg.name}-{pkg_aur.version}'
        if os.path.exists(pkg_sync_path_old):
            rmtree(pkg_sync_path_old)
        if not os.path.exists(pkg_sync_path):
            os.mkdir(pkg_sync_path)
            with open(f'{pkg_sync_path}/info.json', 'w') as jfile:
                pkg_aur.srcpkg = f'{pkg_sync_path}/{pkg.name}.tar.gz'
                json.dump(pkg_aur.__dict__, jfile, indent=4)
            print(f'downloading: {pkg.name}-{pkg_aur.version}')
            self.fetch(f'{self.url}/{pkg_aur.urlpath}', pkg_aur.srcpkg)

    # def compare_version(self, pkg ,pkg_new):
    #     local_ver, local_rel = pkg_aur.get('Version', '').rsplit('-', 1)
    #     if local_ver == pkg.get('pkgver') and local_rel == pkg.get('pkgrel'):
    #         print('aktualne')
    #     else:
    #         pass

    @staticmethod
    def fetch(url, dest):
        try:
            web = urllib.request.urlopen(url)
            with open(dest, 'wb') as download_file:
                download_file.write(web.read())
        except urllib.error.HTTPError as err:
            print(err)

    def update(self):
        for aur_pkg in self.pacmandb.find_aur_pkg():
            self.sync_pkg(aur_pkg)

    def upgrade(self):
        for p in self.upgredable():
            self.install(p)

    def upgredable(self):
        ret = list()
        for aur_pkg in self.pacmandb.find_aur_pkg():
            for pkg in os.listdir(self.aur_sycn_path):
                local_pkg = Pkg()
                local_pkg.name, local_pkg.version, local_pkg.rel = pkg.rsplit('-', 2)
                if aur_pkg.name == local_pkg.name:
                    if not aur_pkg == local_pkg:
                        ret.append(local_pkg)
        return ret

    def get_deps_list(self, pkg):
        """getDeps : return depenecies list"""
        self.pacmandb.loadLocalDb()  # refresh local db
        self.pacmandb.loadSyncDb()  # refresh syncdb
        pacman_deps = dict()
        aur_deps = dict()
        for dep in pkg.depends:
            if dep in self.pacmandb.dbLocal:
                pass  # skip installed
            elif dep in self.pacmandb.dbSync:
                pacman_deps[dep] = True
            else:
                aur_deps[dep] = True

        return pacman_deps, aur_deps

    def install(self, pkg):
        """insallPkg"""
        print(pkg, os.listdir(self.aur_sycn_path), str(pkg) in os.listdir(self.aur_sycn_path))
        if str(pkg) not in os.listdir(self.aur_sycn_path):
            # pname, pver, prel = pkgname.rsplit('-', 2)
            # if pkgname == pname:
            #     ret = subprocess.run(['tar', 'xf', os.path.join(self.dbPath, 'sync', dbname)], stdout=subprocess.PIPE)
            print(f'syncing {pkg}')
            self.sync_pkg(pkg)

        print(self.get_deps_list(pkg))

    def install_pkg(self, pkg):
        pass

    def search(self, pkg_name):
        """search:
        :param pkg_name : name
        :returns : list of found packages"""
        _search_result = list()
        ret = self._get({'type': 'search', 'arg': pkg_name})
        for p in ret.get('results', list()):
            pkg = Pkg()
            pkg.from_aur(p)
            _search_result.append(pkg)
        return _search_result

    def info(self, pkg_name):
        """info:
        Args:
            :param pkg_name : name
        Returns
        :returns : dict witch pkg info"""
        ret = self._get({'type': 'info', 'arg': pkg_name})
        if ret.get('resultcount', 0) > 0:
            _info = Pkg()
            _info.from_aur(ret.get('results')[0])
            return _info
        else:
            return list()

    def _get(self, query):
        query['v'] = self.proto_version
        _query = '&'.join([f'{q}={query[q]}' for q in query])
        try:
            url = urllib.request.urlopen(f'{self.rpc}?{_query}')
            if url.getcode() == 200:
                return self._jload(url.read().decode())
        except urllib.error.HTTPError as err:
            print(err)

    @staticmethod
    def _jload(data):
        try:
            return json.loads(data)
        except json.JSONDecodeError:
            raise ServerError('data')


class ServerError(Exception):
    pass
