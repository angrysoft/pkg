#!/usr/bin/env python3

# Copyright 2015 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com>
# pkg
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import subprocess
from pkgman.pacman import Pacman
from pkgman.pacman import PacmanDb
from pkgman.aur import Aur


class Pkgman:

    def __init__(self, config):
        self.pacman = Pacman()
        self.pacmanDb = PacmanDb()
        self.aur = Aur()
        self._verbose = config.verbose
        self._noConfig = config.noconfirm
        self.power = config.power
        self.green = '\033[0;32m'
        self.white = '\033[1;37m'
        self.blue = '\033[0;34m'
        self.cyan = '\033[0;36m'
        self.nocolor = '\033[0m'
        self._search = config.search
        self._remove = config.remove
        self._info = config.info
        self._update = config.update
        self._upgrade = config.upgrade
        self.packages = config.packages
        print(config)

    def run(self):
        """run"""
        if self._update:
            self.update()

        if self._search:
            self.search()
        elif self._info:
            self.info()
        elif self._upgrade:
            self.upgrade()
        else:
            if self.packages:
                self.install()

    def setPacmanOptions(self):
        self.pacman.verbose = self._verbose
        self.pacman.noConfirm = self._noconfirm

    def setAurOptions(self):
        pass

    def search(self):
        """search"""
        self.pacman.search(self.packages[0])
        self.printAurSearch(self.packages[0])

    def printAurSearch(self, pkgname):
        self.pacmanDb.loadLocalDb()
        for pkg in self.aur.search(pkgname):
            installed = ''
            if pkg.name in self.pacmanDb.dbLocal:
                installed = f' {self.cyan}[zainstalowano]'
            print(f'{self.green}aur/{self.white}{pkg.name} {self.green}{pkg.version}{installed}'
                  f'{self.nocolor}')
            print(f'    {pkg.description}')

    def upgrade(self):
        """upgrade"""
        # self.pacman.upgrade()
        self.aur.upgrade()
    
    def update(self):
        """update"""
        # self.pacman.update()
        self.aur.update()
        
    def info(self):
        """info"""
        self.pacmanDb.loadSyncDb()
        if self.packages[0] in self.pacmanDb.dbSync:
            self.pacman.info(self.packages[0])
        else:
            self.printAurPkgInfo(self.packages[0])

    def install(self):
        """info"""
        self.pacmanDb.loadSyncDb()
        pacmanPkgs = list()
        aurPkgs = list()
        for pkg in self.packages:
            if pkg in self.pacmanDb.dbSync:
                pacmanPkgs.append(pkg)
            else:
                aurPkgs.append(pkg)
        if pacmanPkgs:
            self.pacman.install(pacmanPkgs)

        if aurPkgs:
            self.installFromAur(aurPkgs)

    def installFromAur(self, pkgs):
        """installAUR"""
        print('Aur : {}'.format(', '.join(pkgs)))
        for _pkg in pkgs:
            self.aur.install(self.aur.info(_pkg))

    def isRoot(self):
        """isRoot: rerturn bool"""
        pass

    def printAurPkgInfo(self, pkgname):
        """printAurPkgInfo"""
        info = self.aur.info(pkgname)
        if info:
            self._printInfo('Repozytorium', 'aur')
            self._printInfo('Nazwa', info.name)
            self._printInfo('Wersja', info.version)
            self._printInfo('Opis', info.description)
            self._printInfo('Adres url', info.url)
            self._printInfo('Licencja', info.license)
            self._printInfo('Zależy od', info.depends)
            self._printInfo('Opcjonalne Zależności', info.optdepends)
            self._printInfo('Zależności budowania', info.makedepends)
            self._printInfo('Konfliktuje z', info.conflicts)
            self._printInfo('Provides', info.provides)
            self._printInfo('Zastępuje', info.replaces)
            self._printInfo('Autor pakietu', info.maintainer)

    def _printInfo(self, name, value):
        """_printInfo"""
        if not name or not value:
            return
        if type(value) == list:
            value = ' '.join(value)
        print('{white}{name:<21} : {nocolor}{value}'.format(white=self.white,
                                                            nocolor=self.nocolor,
                                                            name=name,
                                                            value=value))

    def sysUpdate(self):
        self.pacman.systemUpgrade()
        if self.power == 'poweroff':
            subprocess.call(['systemctl', 'poweroff', '-i'])
        elif self.power == 'reboot':
            subprocess.call(['systemctl', 'reboot', '-i'])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage='%(prog)s [options] [packagees]')
    parser.add_argument('packages', nargs='*')
    parser.add_argument('-v', '--verbose', action="store_true", help="Gadatliwy")
    parser.add_argument('-n', '--noconfirm', action="store_true", help='wykonaj bez potwierdzania')
    parser.add_argument('-s', '--search', action="store_true", help='szukaj')
    parser.add_argument('-y', '--update', action="store_true", help='uaktualnij bazę')
    parser.add_argument('-u', '--upgrade', action="store_true", help='uaktualnij')
    parser.add_argument('-i', '--info', action="store_true", help='informacje o pakiecie')
    parser.add_argument('-r', '--remove', action="store_true", help='usuń pakiety')
    parser.add_argument('--reboot', action='store_const', const='reboot', dest='power', help='Restartuj System')
    parser.add_argument('--poweroff', action='store_const', const='poweroff', dest='power', help='Wyłącz System')

    #subparsers = parser.add_subparsers()

    # query_opts = optparse.OptionGroup(
    #    parser, 'Query Options',
    #    'These options control the query mode.',
    # )
    # query_opts.add_argument('-l', action='store_const', const='list', dest='query_mode',
    #                      help='List contents')
    # query_opts.add_argument('-f', action='store_const', const='file', dest='query_mode',
    #                      help='Show owner of file')
    # query_opts.add_argument('-a', action='store_const', const='all', dest='query_mode',
    #                      help='Show all packages')
    # parser.add_argument_group(query_opts)
    args = parser.parse_args()
    p = Pkgman(args)
    p.run()


