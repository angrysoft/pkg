#!/usr/bin/env python
import configparser
from pkgman.pacman import PacmanDb
from pkgman.aur import Aur

config = configparser.ConfigParser(allow_no_value=True)
config.read('/etc/pacman.conf')

db = PacmanDb()
db.loadLocalDb()
print(config.sections())
for repo in config.sections():
    if repo == 'options':
        continue
    print('loading: ', repo)
    db.loadTarDb('{}.db'.format(repo))
a = Aur()
for x in db.dbLocal:
    if x not in db.dbSync:
        print(x)
#print(a.search('pycharm-community'))
#a.syncPkg('arch-audit')

#print(a.info('pycharm-community'))
#print(a.info('urh'))
#print(db.parseDescFile('/var/lib/pacman/local/gvim-7.4.944-2/desc'))
#print(db.dbLocal)
#print(db.dbLocal)
#for x in db.dbLocal:
#    print(x)

#for x in db.dbSync:
#    print(x, db.dbSync[x])
