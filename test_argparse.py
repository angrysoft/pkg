#!/usr/bin/env python3
import argparse

class FooAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(FooAction, self).__init__(option_strings, dest, nargs, **kwargs)
        self.pkgs = None

    def __call__(self, parser, namespace, values, option_string=None):
        self.pkgs = values
        setattr(namespace, self.dest, values)
        print('call', self.dest)

    def setOptions(self, opt):
        print(opt)

    def doAction(self):
        print(self.dest)


parser = argparse.ArgumentParser(usage='%(prog)s [options] [packagees]')
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-f', '--foo', nargs='+' , action=FooAction)
parser.add_argument('-c', '--coo', nargs='+' , action=FooAction)
args = parser.parse_args()
#print(args)